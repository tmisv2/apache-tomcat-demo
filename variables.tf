variable "app_name" {}
variable "aws_region" {
  default = "eu-west-1"
}
variable "vpc_id" {}
variable "web_instance_type"{}
variable "app_instance_type"{}

variable "web_fqdn" {}

variable "min_app_instances" {}
variable "max_app_instances" {}
variable "desired_app_instances" {}

variable "min_web_instances" {}
variable "max_web_instances" {}
variable "desired_web_instances" {}

variable "xload_account" {}
variable "xload_accountuser" {}

variable "key_name" {}