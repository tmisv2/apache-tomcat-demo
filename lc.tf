resource "aws_launch_configuration" "app" {
  name_prefix = "${var.app_name}-app"
  image_id = data.aws_ami.linux.id
  instance_type = var.app_instance_type
  user_data = data.template_file.app.rendered
  iam_instance_profile = aws_iam_instance_profile.app.id
  key_name = var.key_name
  enable_monitoring = true
  ebs_optimized = true
  security_groups = [aws_security_group.app.id]
  root_block_device {
    volume_type = "gp2"
    volume_size = 20
    delete_on_termination = true
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_launch_configuration" "web" {
  name_prefix = "${var.app_name}-web"
  image_id = data.aws_ami.linux.id
  instance_type = var.web_instance_type
  user_data = data.template_file.web.rendered
  iam_instance_profile = aws_iam_instance_profile.app.id
  key_name = var.key_name
  enable_monitoring = true
  ebs_optimized = true
  security_groups = [aws_security_group.web.id]
  root_block_device {
    volume_type = "gp2"
    volume_size = 20
    delete_on_termination = true
  }
  lifecycle {
    create_before_destroy = true
  }
}

