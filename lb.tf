# LB for web layer
resource "aws_elb" "web" {
  name               = "${var.app_name}-web"
  availability_zones = data.aws_availability_zones.all.names

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 443
    lb_protocol       = "https"
    ssl_certificate_id = aws_acm_certificate_validation.web.certificate_arn
  }



  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  security_groups = [aws_security_group.web_elb.id]


  tags = {
    Name = "${var.app_name}-web"
  }
}

# LB for app layer
resource "aws_elb" "app" {
  name               = "${var.app_name}-app"
  availability_zones = data.aws_availability_zones.all.names

  #listener for x-shirt admin
  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 443
    lb_protocol       = "https"
    ssl_certificate_id = aws_acm_certificate_validation.app.certificate_arn
  }

  #listener for web layer
  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 8080
    lb_protocol       = "http"
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  security_groups = [aws_security_group.app_elb.id]

  tags = {
    Name = "${var.app_name}-app"
  }
}



