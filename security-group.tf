# Security Group for APP ELB
resource "aws_security_group" "app_elb" {
  name = "${var.app_name}-app-elb"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    #this cidr block should be changed to the internal network for x-shirt as they should have a VPN set up for accessing
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.app_name}-app-elb"
  }
}



resource "aws_security_group" "app" {
  name = "${var.app_name}-app"
  description = "app SG for ${var.app_name}"
  vpc_id = var.vpc_id

  ingress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.app_name}-app"
  }
}

## Security Group for WEB ELB
resource "aws_security_group" "web_elb" {
  name = "${var.app_name}-web-elb"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.app_name}-web-elb"
  }
}

resource "aws_security_group" "web" {
  name = "${var.app_name}-web"
  description = "web SG for ${var.app_name}"
  vpc_id = var.vpc_id

  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.app_name}-web"
  }
}

resource "aws_security_group" "db" {
  name = "${var.app_name}-db"
  description = "db SG for ${var.app_name}"
  vpc_id = var.vpc_id
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.app_name}-db"
  }
}


# these rules depend on both security groups so separating them allows them
# to be created after both

resource "aws_security_group_rule" "ingress-from-web" {
  security_group_id        = aws_security_group.app_elb.id
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  type                     = "ingress"
  source_security_group_id = aws_security_group.web.id
}

resource "aws_security_group_rule" "ingress-from-app" {
  security_group_id        = aws_security_group.db.id
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  type                     = "ingress"
  source_security_group_id = aws_security_group.app.id
}