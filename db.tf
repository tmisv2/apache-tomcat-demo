locals {
  db_creds = jsondecode(
  data.aws_secretsmanager_secret_version.creds.secret_string
  )
}

resource "aws_db_instance" "db" {
  name              = "xshirt"
  engine            = "postgres"
  engine_version    = "12.3"
  instance_class    = "db.t3.micro"
  allocated_storage = 20
  storage_encrypted = true
  # Set the secrets from AWS Secrets Manager
  username = local.db_creds.user
  password = local.db_creds.pass
  vpc_security_group_ids = [aws_security_group.db.id]
  #final_snapshot_identifier = "${var.app_name}-final"
  #copy_tags_to_snapshot = true
  skip_final_snapshot = true
  tags = {
    Name = var.app_name
  }
}