resource "aws_iam_instance_profile" "app" {
  name = var.app_name
  role = aws_iam_role.app.name
}

#standard EC2 Trust Doc
resource "aws_iam_role" "app" {
  name = "${var.app_name}-role"
  assume_role_policy =<<EOF
{
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "ec2.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
}
EOF
}

resource "aws_iam_policy" "logs" {
  name        = "${var.app_name}-logs"
  description = "Allow log access to logs bucket"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.logs.arn}",
        "${aws_s3_bucket.logs.arn}/*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "logs" {
  role       = aws_iam_role.app.name
  policy_arn = aws_iam_policy.logs.arn
}