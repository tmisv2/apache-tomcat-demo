app_name="x-shirt"
aws_region = "eu-central-1"

vpc_id = "vpc-7196641a" #using default vpc for the purposes of demo
web_fqdn = "x-shirt.tom-currie.co.uk"

#web instances
web_instance_type = "t3a.micro"
min_web_instances = 2
max_web_instances = 12 #to ensure scaling for 500% increase in traffic during sales
desired_web_instances = 2


#app instances
app_instance_type = "t3a.micro"
min_app_instances = 2
max_app_instances = 12 #to ensure scaling for 500% increase in traffic during sales
desired_app_instances = 2

key_name = "key-de"


#x-load account & username for external access to bucket
xload_account = "123456789123"
xload_accountuser = "test"