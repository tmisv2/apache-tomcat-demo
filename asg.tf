resource "aws_autoscaling_group" "app" {
  name                      = "${var.app_name}-app"
  max_size                  = var.max_app_instances
  min_size                  = var.min_app_instances
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = var.desired_app_instances
  force_delete              = true
  launch_configuration      = aws_launch_configuration.app.name
  availability_zones = data.aws_availability_zones.all.names
  load_balancers = [aws_elb.app.id]

  tag {
    key                 = "Name"
    value               = "${var.app_name}-app"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_group" "web" {
  name                      = "${var.app_name}-web"
  max_size                  = var.max_web_instances
  min_size                  = var.min_web_instances
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = var.desired_web_instances
  force_delete              = true
  launch_configuration      = aws_launch_configuration.web.name
  availability_zones = data.aws_availability_zones.all.names
  load_balancers = [aws_elb.web.id]

  tag {
    key                 = "Name"
    value               = "${var.app_name}-web"
    propagate_at_launch = true
  }
}

