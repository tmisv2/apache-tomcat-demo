
resource "aws_s3_bucket" "x-load_bucket" {
  bucket = "${var.app_name}-x-load"
  acl    = "private"
  tags = {
    Name = "${var.app_name}-x-load"
  }
}

resource "aws_s3_bucket" "logs" {
  bucket = "${var.app_name}-logs"
  acl    = "private"
  tags = {
    Name = "${var.app_name}-logs"
  }
}

# Give access to x-load bucket for other account for AccountBUserName
#resource "aws_s3_bucket_policy" "external_access" {
#  bucket = aws_s3_bucket.x-load_bucket.id
#  policy = <<POLICY
#{
#   "Version": "2012-10-17",
#    "Statement": [
#        {
#            "Effect": "Allow",
#            "Principal": {
#                "AWS": "arn:aws:iam::${var.xload_account}:user/${var.xload_accountuser}"
#            },
#            "Action": [
#                "s3:GetObject",
#                "s3:PutObject"
#            ],
#            "Resource": [
#                "${aws_s3_bucket.x-load_bucket.arn}"
#            ]
#        }
#    ]
#}
#POLICY
#}