# AWS Region for S3 and other resources, this can be another region.
provider "aws" {
  region = var.aws_region
}