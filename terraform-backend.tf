#Store statefile in S3
terraform {
  backend "s3" {
    bucket = "tom-currie-statefiles"
    key    = "x-shirt/terraform.tfstate"
    region = "eu-west-1"
  }
}