data "aws_route53_zone" "main" {
  name         = "tom-currie.co.uk"
  private_zone = false
}

#A record points to LB.
resource "aws_route53_record" "web" {
  zone_id  = data.aws_route53_zone.main.zone_id
  name     = var.web_fqdn
  type     = "A"
  allow_overwrite = true

  alias {
    name                   = aws_elb.web.dns_name
    zone_id                = aws_elb.web.zone_id
    evaluate_target_health = true
  }
}

#A record points to LB.
resource "aws_route53_record" "app" {
  zone_id  = data.aws_route53_zone.main.zone_id
  name     = "app-${var.web_fqdn}"
  type     = "A"
  allow_overwrite = true

  alias {
    name                   = aws_elb.app.dns_name
    zone_id                = aws_elb.app.zone_id
    evaluate_target_health = true
  }
}


