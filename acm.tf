
resource "aws_acm_certificate" "web" {
  domain_name       = var.web_fqdn
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "app" {
  domain_name       = "app-${var.web_fqdn}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}


#validate using route53 record
resource "aws_route53_record" "web_validation" {
  for_each = {
  for dvo in aws_acm_certificate.web.domain_validation_options : dvo.domain_name => {
    name    = dvo.resource_record_name
    record  = dvo.resource_record_value
    type    = dvo.resource_record_type
    zone_id = data.aws_route53_zone.main.zone_id
  }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = each.value.zone_id
}

#validate using route53 record
resource "aws_route53_record" "app_validation" {
  for_each = {
  for dvo in aws_acm_certificate.app.domain_validation_options : dvo.domain_name => {
    name    = dvo.resource_record_name
    record  = dvo.resource_record_value
    type    = dvo.resource_record_type
    zone_id = data.aws_route53_zone.main.zone_id
  }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = each.value.zone_id
}


#Certificate Validation
resource "aws_acm_certificate_validation" "web" {
  certificate_arn         = aws_acm_certificate.web.arn
  validation_record_fqdns = [for record in aws_route53_record.web_validation: record.fqdn]
}

#Certificate Validation
resource "aws_acm_certificate_validation" "app" {
  certificate_arn         = aws_acm_certificate.app.arn
  validation_record_fqdns = [for record in aws_route53_record.app_validation: record.fqdn]
}
